#!/bin/bash

ansible -i hosts computenodes  -u debian -b -m ping
ansible -i hosts computenodes  -u debian -b -m shell -a '/usr/bin/id; /usr/bin/hostname -f; /usr/bin/ip addr'
ansible -i hosts computenodes  -u debian -b -m setup
