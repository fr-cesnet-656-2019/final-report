# Nastavení Python prostředí pro výpočty

## Použití Python VirtualEnv s VirtualEnvWrapper
Všichni uživatelé mají připraven k použití nástroj [`VirtualEnvWrapper`](https://virtualenvwrapper.readthedocs.io/en/latest/), tj. uživatelsky příjemné ovládání [Python VirtualEnv](https://virtualenv.pypa.io/en/latest/) prostředí.

### Základní ovládání
 - Založení nového virtuálního prostředí
   `mkvirtualenv -p $(type -p python3) comp_env`
 - Výpis dostupných virtuálních prostředí
   `lsvirtualenv`
  - Zrušení virtuálního prostředí
    `rmvirtualenv comp_env`

### Použití
Typický postup vytvoření nového virtuálního prostředí zahrnuje použití existujícího souboru `requirements.txt` se seznamem Python balíků, které mají být v prostředí dostupné:
```bash
$ mkvirtualenv -p $(type -p python3) comp_env
$ pip install -r requirements.txt
```
Po vytvoření je prostředí automaticky aktivováno – příkazy `pyhton` apod. jsou z lokálního virtuálního prostředí. Stejně tak jsou lokálně do virtuálního prostředí instalovány další Python balíky.
```bash
$ type python
python is /mnt/data/home/franta/.virtualenvs/comp_env/bin/python
$ type pip
pip is /mnt/data/home/franta/.virtualenvs/comp_env/bin/pip
$ python --version
Python 3.7.3
```
Práci ve virtuálním prostředí je možné ukončit příkazem `deactivate`. (Jiné) virtuální prostředí můžeme opět aktivovat příkazem `workon <název prostředí>`.
```bash
$ deactivate 
$ type python
python is /usr/bin/python
$ workon comp_env
$ type python
python is /mnt/data/home/franta/.virtualenvs/comp_env/bin/python
```
***Nezapomeňte, že prostředí je aktivní jen pro aktuálního shell***, *je tedy nutné uvést příkaz `workon` pokaždé, když např. otevřete nové okno terminálu apod.*

### Instalace software
Zásadní je, že uvnitř virtálního prostředí (po aktivaci pomocí `workon <název prostředí>`) můžete instalovat software pomocí nástroje `pip` dovnitř toho prostředí. Tj. bez toho, abyste ovlivnili systémovou instalaci Pythonu pro ostatní uživatele, respektive i bez toho, abyste ovlivnili balíky instalované ve svých vlastní jiných virutálních prostředí. Používání virtuálního prostředí je tedy ideální pro vývoj – snadno si vytvoříte více různých sad Python balíčků / verzí balíčků a můžete mezi nimi rychle přepína příkazy `workon` a `deactivate`.

Po aktivaci prostředí můžete instalovat konkrétní balík pomocí `pip`:
```bash
$ pip install -U pip scikit-learn
```

#### Uložení definice prostředí (pro Git apod.)
Aktuální stav prostředí, tj. seznam balíků a jejich verzí, snadno zjistíte příkazem `pip freeze`. Seznam můžete uložit do souboru `requirements.txt`, a ten následně uložit např. do Gitu. Tím pro sebe i ostatní vývojáře vytvoříte definici prostředí, ve kterém je váš kód otestován.
```bash
$ pip freeze
$ pip freeze > requirements.txt
# git add requirements.txt && git commit
```

#### Obnovení prostředí (z Git apod.)
Pokud následně potřebujete svůj software zprovoznit jinde, respektive potřebuje nějaký jiný uživatel vytvořit stejné běhové prostředí, stačí získat soubor `requirements.txt` (typicky staženním z Git současně s Python kódem, který chce spouštět) a instalovat Python balíky podle něj:
```bash
# git pull
$ pip install -r requirements.txt
```

